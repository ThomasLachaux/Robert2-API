<?php
declare(strict_types=1);

namespace Robert2\Tests;

use PHPUnit\Framework\TestCase;

final class FunctionsTest extends TestCase
{
    public function testGetExecutionTime(): void
    {
        $this->assertNotEmpty(getExecutionTime());
    }

    public function testSnakeToCamelCase(): void
    {
        $this->assertEquals('unTest', snakeToCamelCase('un_test'));
        $this->assertEquals('SecondTest', snakeToCamelCase('second_test', true));
    }
}
