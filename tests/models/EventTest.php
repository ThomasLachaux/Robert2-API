<?php
declare(strict_types=1);

namespace Robert2\Tests;

use Robert2\API\Models;

final class EventTest extends ModelTestCase
{
    public function setup(): void
    {
        parent::setUp();

        $this->model = new Models\Event();
    }

    public function testGetAll(): void
    {
        $this->model->setPeriod('2018-01-15', '2018-12-18');
        $result = $this->model->getAll()->get()->toArray();
        $this->assertEquals([
            [
                'id'           => 3,
                'user_id'      => 1,
                'title'        => "Avant-premier événement",
                'description'  => null,
                'start_date'   => "2018-12-15 00:00:00",
                'end_date'     => "2018-12-16 23:59:59",
                'is_confirmed' => null,
                'location'     => "Brousse",
                'created_at'   => null,
                'updated_at'   => null,
                'deleted_at'   => null,
            ],
            [
                'id'           => 1,
                'user_id'      => 1,
                'title'        => "Premier événement",
                'description'  => null,
                'start_date'   => "2018-12-17 00:00:00",
                'end_date'     => "2018-12-18 23:59:59",
                'is_confirmed' => null,
                'location'     => "Gap",
                'created_at'   => null,
                'updated_at'   => null,
                'deleted_at'   => null,
            ],
            [
                'id'           => 2,
                'user_id'      => 1,
                'title'        => "Second événement",
                'description'  => null,
                'start_date'   => "2018-12-18 00:00:00",
                'end_date'     => "2018-12-19 23:59:59",
                'is_confirmed' => null,
                'location'     => "Lyon",
                'created_at'   => null,
                'updated_at'   => null,
                'deleted_at'   => null,
            ]
        ], $result);
    }

    public function testGetMissingMaterials(): void
    {
        // - No missing materials for event #3
        $result = $this->model->getMissingMaterials(3);
        $this->assertNull($result);

        // - Get missing materials of event #1
        $result = $this->model->getMissingMaterials(1);
        $this->assertCount(1, $result);
        $this->assertEquals('DBXPA2', $result[0]['reference']);
        $this->assertEquals(-1, $result[0]['remaining_quantity']);
    }

    public function testGetMaterials(): void
    {
        $Event = $this->model::find(1);
        $results = $Event->materials;
        $this->assertCount(3, $results);
        $expected = [
            'id'                    => 4,
            'name'                  => 'Showtec SDS-6',
            'description'           => "Console DMX (jeu d'orgue) Showtec 6 canaux",
            'reference'             => 'SDS-6-01',
            'park_id'               => 1,
            'category_id'           => 2,
            'sub_category_id'       => 4,
            'rental_price'          => 15.95,
            'stock_quantity'        => 2,
            'out_of_order_quantity' => null,
            'replacement_price'     => 59.0,
            'serial_number'         => '1212121-5',
            'tags'                  => [],
            'attributes'            => [
                [
                    'id'    => 4,
                    'name'  => 'Conforme',
                    'type'  => 'boolean',
                    'unit'  => null,
                    'value' => true,
                ],
                [
                    'id'    => 3,
                    'name'  => 'Puissance',
                    'type'  => 'integer',
                    'unit'  => 'W',
                    'value' => 60,
                ],
                [
                    'id'    => 1,
                    'name'  => 'Poids',
                    'type'  => 'float',
                    'unit'  => 'kg',
                    'value' => 3.15,
                ],
            ],
            'pivot' => [
                'event_id'    => 1,
                'material_id' => 4,
                'quantity'    => 1,
            ],
        ];
        $this->assertEquals($expected, $results[0]);
    }

    public function testGetAssignees(): void
    {
        $Event   = $this->model::find(1);
        $results = $Event->assignees;
        $this->assertCount(2, $results);
    }

    public function testGetBeneficiaries(): void
    {
        $Event   = $this->model::find(1);
        $results = $Event->beneficiaries;
        $this->assertEquals([
            [
                'id'         => 3,
                'first_name' => 'Client',
                'last_name'  => 'Benef',
                'full_name'  => 'Benef Client',
                'country'    => null,
                'pivot'      => ['event_id' => 1, 'person_id' => 3],
            ]
        ], $results);
    }

    public function testSetPeriod()
    {
        // - Set period to current year
        $this->model->setPeriod(null, null);
        $results = $this->model->getAll()->get()->toArray();
        $this->assertCount(0, $results);

        // - Set period to 2018
        $this->model->setPeriod('2018-01-01', '2018-12-31');
        $results = $this->model->getAll()->get()->toArray();
        $this->assertCount(3, $results);
        $this->assertEquals('Avant-premier événement', $results[0]['title']);

        // - Set period to last dec. 2018
        $this->model->setPeriod('2018-12-19', '2018-12-31');
        $results = $this->model->getAll()->get()->toArray();
        $this->assertCount(1, $results);
        $this->assertEquals('Second événement', $results[0]['title']);
    }
}
