<?php
declare(strict_types=1);

namespace Robert2\Tests;

use PHPUnit\Framework\TestCase;

use Robert2\API\Config;
use Robert2\Fixtures;

class ModelTestCase extends TestCase
{
    protected $settings;

    protected $Fixtures  = null;

    public function setup(): void
    {
        parent::setUp();

        try {
            $this->settings = Config\Config::getSettings();

            Fixtures\RobertFixtures::resetDataWithDump();
        } catch (\Exception $e) {
            $this->fail(sprintf("Unable to reset fixtures: %s", $e->getMessage()));
        }
    }
}
