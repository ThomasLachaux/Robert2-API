<?php
declare(strict_types=1);

/**
 * Check wether current run is in TEST mode
 *
 * @return bool True if script is running in TEST mode
 *
 * @codeCoverageIgnore
 */
function isTestMode(): bool
{
    return getenv('PHP_ROBERT2_TESTING') === 'TESTS';
}

/**
 * Print a variable, and exit current script (or not)
 *
 * @param var mixed: Variable to monitor
 * @param exit bool: Wether to exit after display or not (default TRUE)
 * @return void
 *
 * @codeCoverageIgnore
 */
function debug($var = null, bool $exit = true): void
{
    $wrap = ['<pre>', '</pre>'];

    if (isTestMode()) {
        $wrap = ["\n\033[35m", "\033[0m\n"];
    }

    echo $wrap[0];
    if (is_array($var) || is_object($var) || is_callable($var)) {
        print_r($var);
    } else {
        var_dump($var);
    }
    echo $wrap[1];

    if ($exit) {
        exit;
    }
}

/**
 * Get elapsed time since php script first launched, or a custom start microtime
 *
 * @param start float: A custom start microtime
 * @return string The elapsed time, in seconds.
 */
function getExecutionTime(?float $start = null): string
{
    $start       = $start ?: (float)$_SERVER['REQUEST_TIME_FLOAT'];
    $elapsedTime = microtime(true) - $start;

    return number_format($elapsedTime, 3) . "s";
}

/**
 * Transform any snake_case string into camelCase string
 *
 * @param str string: The string to transform
 * @param capitalizeFirstLetter bool: Wether to capitalize the first letter or not
 * @return string
 */
function snakeToCamelCase(string $str, bool $capitalizeFirstLetter = false): string
{
    $string = str_replace('_', '', ucwords($str, '_'));

    if (!$capitalizeFirstLetter) {
        return lcfirst($string);
    }

    return $string;
}
