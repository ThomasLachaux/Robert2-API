<?php

// - GLOBAL CONSTANTS, for use all over the application

// - Paths
if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}
if (!defined('ROOT_FOLDER')) {
    define('ROOT_FOLDER', dirname(dirname(dirname(__FILE__))));
}
if (!defined('PUBLIC_FOLDER')) {
    define('PUBLIC_FOLDER', ROOT_FOLDER . DS . 'public');
}

// - Errors codes
define('ERROR_VALIDATION', 400);
define('ERROR_UNAUTHORIZED', 401);
define('ERROR_NOT_FOUND', 404);
define('ERROR_NOT_ALLOWED', 405);
define('ERROR_DUPLICATE', 409);
define('ERROR_SERVER', 500);

// - Success codes
define('SUCCESS_OK', 200);
define('SUCCESS_CREATED', 201);
