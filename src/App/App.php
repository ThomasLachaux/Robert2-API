<?php
declare(strict_types=1);

namespace Robert2\API;

use Robert2\API\Config as Config;
use Robert2\API\Errors as Errors;
use Robert2\API\Middlewares as Middlewares;

class App
{
    private $app;

    public function __construct()
    {
        $this->settings = Config\Config::getSettings();

        $this->app = new \Slim\App([
            'settings' => $this->settings
        ]);

        $this->_setContainer();
        $this->_setAppRoutes();
        $this->_setMiddlewares();
    }

    public function get(bool $isTest): \Slim\App
    {
        $container = $this->app->getContainer();
        $settings  = $container->get('settings');

        // @codeCoverageIgnoreStart
        if (!$isTest && $settings['enableCORS']) {
            $this->_enableCORS();
        } elseif ($isTest) {
            $settings->replace([
                'displayErrorDetails' => true,
                'routerCacheFile'     => false,
            ]);
        }
        // @codeCoverageIgnoreEnd

        return $this->app;
    }

    /**
     * @codeCoverageIgnore
     */
    private function _enableCORS(): void
    {
        $this->app->options('/api/{routes:.+}', function ($request, $response, $args) {
            return $response;
        });

        $appSettings = $this->settings;

        $this->app->add(function ($request, $response, $next) use ($appSettings) {
            $_response = $next($request, $response);
            return $_response
                ->withHeader('Access-Control-Allow-Origin', $appSettings['apiUrl'])
                ->withHeader(
                    'Access-Control-Allow-Headers',
                    'X-Requested-With, Content-Type, Accept, Origin, Authorization'
                )
                ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
        });
    }

    private function _setContainer(): void
    {
        $container = $this->app->getContainer();

        $container->register(new \Projek\Slim\MonologProvider());

        $container = $this->_setHttpCachePovider($container);
        $container = $this->_setErrorHandlers($container);
        $container = $this->_setControllers($container);
    }

    private function _setHttpCachePovider(\Slim\Container $container): \Slim\Container
    {
        // @codeCoverageIgnoreStart
        $container['cache'] = function () {
            return new \Slim\HttpCache\CacheProvider();
        };
        // @codeCoverageIgnoreEnd

        return $container;
    }

    private function _setErrorHandlers(\Slim\Container $container): \Slim\Container
    {
        // - Error (500) handler
        $container['errorHandler'] = function ($c) use ($container) {
            return new Errors\ErrorHandler($c);
        };
        $container['phpErrorHandler'] = function ($c) use ($container) {
            // @codeCoverageIgnoreStart
            return new Errors\ErrorHandler($c);
            // @codeCoverageIgnoreEnd
        };
        // - Method Not Allowed (405) handler
        $container['notAllowedHandler'] = function ($c) {
            return new Errors\MethodNotAllowedHandler();
        };
        // - Not Found (404) handler
        $container['notFoundHandler'] = function ($c) {
            return new Errors\NotFoundHandler();
        };

        return $container;
    }

    private function _setControllers(\Slim\Container $container): \Slim\Container
    {
        foreach (glob(__DIR__ . '/Controllers/*Controller.php') as $controller) {
            $controllerPath = pathinfo($controller);
            $controllerName = $controllerPath['filename'];

            if ($controllerName === "BaseController") {
                continue;
            }

            $container[$controllerName] = function ($container) use ($controllerName) {
                $controllerClass = "Robert2\\API\\Controllers\\$controllerName";
                return new $controllerClass($container);
            };
        }

        return $container;
    }

    private function _setMiddlewares(): void
    {
        $this->app->add(new Middlewares\Pagination);

        // - JWT security middleware (added last to be executed first) - Skipped for Tests
        if (!isTestMode()) {
            $this->app->add(new Middlewares\Acl);
            $this->app->add(Middlewares\Security::initJwtAuth());
        }
    }

    private function _setAppRoutes()
    {
        $this->app->get('/apidoc', 'HomeController:apidoc')->setName('apidoc');
        $this->app->map(['GET', 'POST'], '/install', 'HomeController:install')->setName('install');

        $this->app->group('/api', function () {
            $routeMethods = (new ApiRouter())->getRoutes();

            foreach ($routeMethods as $method => $routes) {
                foreach ($routes as $route => $controller) {
                    $this->$method($route, $controller);
                }
            }
        });

        $this->app->any('/api/[{path:.*}]', function () {
            throw new Errors\NotFoundException();
        });

        // - All remaining non-API routes should be handled by Front-End Router
        $this->app->get('/[{path:.*}]', 'HomeController:entrypoint');
    }
}
