<?php
declare(strict_types=1);

namespace Robert2\API\Middlewares;

use Firebase\JWT\JWT;
use Tuupola\Middleware\JwtAuthentication;
use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;

use Robert2\API\Config\Config;
use Robert2\API\Config\Acl;

class Security
{
    public static function generateToken(array $user, int $duration = 2): string
    {
        $now     = new \DateTime();
        $expires = new \DateTime(sprintf('now +%d hours', $duration));

        $payload = [
            "iat"  => $now->getTimeStamp(),
            "exp"  => $expires->getTimeStamp(),
            "user" => $user
        ];

        $secret = Config::getSettings('JWTSecret');

        return JWT::encode($payload, $secret, "HS256");
    }

    /**
     * Inits and returns the JwtAuthentication Middleware
     * @codeCoverageIgnore
     */
    public static function initJwtAuth(): JwtAuthentication
    {
        $settings = Config::getSettings();

        $headerName = sprintf(
            'HTTP_%s',
            strtoupper(snake_case($settings['httpAuthHeader']))
        );

        $logger = new Logger('slim');
        $rotating = new RotatingFileHandler(__DIR__ . '/../../var/logs/JWTauth', 0, Logger::DEBUG);
        $logger->pushHandler($rotating);

        return new JwtAuthentication([
            'secure'    => $settings['useHTTPS'],
            'secret'    => $settings['JWTSecret'],
            'header'    => $headerName,
            'cookie'    => $settings['httpAuthHeader'],
            'attribute' => 'jwt',
            'path'      => ['/api'],
            'ignore'    => Acl::PUBLIC_ROUTES,
            "logger"    => $logger,
            'error'     => function ($response, $args) {
                $data = [
                    'success' => false,
                    'error'   => [
                        'message' => $args['message'],
                        'details' => null,
                    ],
                ];
                return $response->withJson($data, ERROR_UNAUTHORIZED);
            },
        ]);
    }
}
