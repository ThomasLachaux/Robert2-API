<?php
declare(strict_types=1);

namespace Robert2\API\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;
use Respect\Validation\Validator as V;

use Robert2\API\Models\Traits\Taggable;

class Material extends BaseModel
{
    use SoftDeletes;
    use Taggable;

    protected $table = 'materials';

    protected $_modelName = 'Material';
    protected $_orderField = 'name';
    protected $_orderDirection = 'asc';

    protected $_allowedSearchFields = ['name', 'reference'];
    protected $_searchField = 'name';

    public function __construct()
    {
        parent::__construct();

        $this->validation = [
            'name'                  => V::notEmpty()->length(2, 191),
            'reference'             => V::notEmpty()->alnum('.,-/_ ')->length(2, 64),
            'park_id'               => V::notEmpty()->numeric(),
            'category_id'           => V::notEmpty()->numeric(),
            'sub_category_id'       => V::optional(V::numeric()),
            'rental_price'          => V::floatVal()->max(999999.99, true),
            'stock_quantity'        => V::intVal()->max(100000),
            'out_of_order_quantity' => V::optional(V::intVal()->max(100000)),
            'replacement_price'     => V::optional(V::floatVal()->max(999999.99, true)),
            'serial_number'         => V::optional(V::alnum('-/*.')->length(2, 64)),
            'is_hidden_on_bill'     => V::optional(V::boolType()),
        ];
    }

    // ——————————————————————————————————————————————————————
    // —
    // —    Relations
    // —
    // ——————————————————————————————————————————————————————

    protected $appends = [
        'tags',
        'attributes',
    ];

    public function Park()
    {
        return $this->belongsTo('Robert2\API\Models\Park')
            ->select(['id', 'name']);
    }

    public function Category()
    {
        return $this->belongsTo('Robert2\API\Models\Category')
            ->select(['id', 'name']);
    }

    public function SubCategory()
    {
        return $this->belongsTo('Robert2\API\Models\SubCategory')
            ->select(['id', 'name', 'category_id']);
    }

    public function Attributes()
    {
        return $this->belongsToMany('Robert2\API\Models\Attribute', 'material_attributes')
            ->using('Robert2\API\Models\MaterialAttributesPivot')
            ->withPivot('value')
            ->select(['attributes.id', 'attributes.name', 'attributes.type', 'attributes.unit']);
    }

    public function Events()
    {
        return $this->belongsToMany('Robert2\API\Models\Event', 'event_materials')
            ->using('Robert2\API\Models\EventMaterialsPivot')
            ->withPivot('quantity')
            ->select(['events.id', 'title', 'start_date', 'end_date', 'is_confirmed']);
    }

    // ——————————————————————————————————————————————————————
    // —
    // —    Mutators
    // —
    // ——————————————————————————————————————————————————————

    protected $casts = [
        'name'                  => 'string',
        'reference'             => 'string',
        'description'           => 'string',
        'park_id'               => 'integer',
        'category_id'           => 'integer',
        'sub_category_id'       => 'integer',
        'rental_price'          => 'float',
        'stock_quantity'        => 'integer',
        'out_of_order_quantity' => 'integer',
        'replacement_price'     => 'float',
        'serial_number'         => 'string',
        'is_hidden_on_bill'     => 'boolean',
        'note'                  => 'string',
    ];

    public function getParkAttribute()
    {
        $park = $this->Park()->first();
        return $park ? $park->toArray() : null;
    }

    public function getCategoryAttribute()
    {
        $category = $this->Category()->first();
        if (!$category) {
            return null;
        }
        $category = $category->toArray();
        unset($category['sub_categories']);

        return $category;
    }

    public function getSubCategoryAttribute()
    {
        $subCategory = $this->SubCategory()->first();
        return $subCategory ? $subCategory->toArray() : null;
    }

    public function getAttributesAttribute()
    {
        $attributes = $this->Attributes()->get();
        if (!$attributes) {
            return null;
        }
        return array_map(function ($attribute) {
            $type = $attribute['type'];
            $value = $attribute['pivot']['value'];
            if ($type === 'integer') {
                $value = (int)$value;
            }
            if ($type === 'float') {
                $value = (float)$value;
            }
            if ($type === 'boolean') {
                $value = $value === 'true' || $value === '1';
            }
            $attribute['value'] = $value;

            unset($attribute['pivot']);
            return $attribute;
        }, $attributes->toArray());
    }

    public function getEventsAttribute()
    {
        $events = $this->Events()->get();
        return $events ? $events->toArray() : null;
    }

    // ——————————————————————————————————————————————————————
    // —
    // —    Setters
    // —
    // ——————————————————————————————————————————————————————

    protected $fillable = [
        'name',
        'reference',
        'description',
        'park_id',
        'category_id',
        'sub_category_id',
        'rental_price',
        'stock_quantity',
        'out_of_order_quantity',
        'replacement_price',
        'serial_number',
        'is_hidden_on_bill',
        'note',
    ];

    // ------------------------------------------------------
    // -
    // -    Custom Methods
    // -
    // ------------------------------------------------------

    public function recalcQuantitiesForPeriod(
        array $data,
        string $start,
        string $end,
        ?int $exceptEventId = null
    ): array {
        if (empty($data)) {
            return [];
        }

        $Event = new Event();
        $events = $Event->setPeriod($start, $end)->getAll();
        if ($exceptEventId) {
            $events = $events->where('id', '!=', $exceptEventId);
        }
        $events = $events->with('Materials')->get()->toArray();

        foreach ($data as $index => $material) {
            $materialId = $material['id'];
            $remainingQuantity = (int)$material['stock_quantity'] - (int)$material['out_of_order_quantity'];

            foreach ($events as $event) {
                $eventMaterialIndex = array_search(
                    $materialId,
                    array_column($event['materials'], 'id')
                );
                if ($eventMaterialIndex === false) {
                    continue;
                }
                $eventMaterial = $event['materials'][$eventMaterialIndex];
                $eventMaterialQuantity = $eventMaterial['pivot']['quantity'];

                $remainingQuantity -= $eventMaterialQuantity;
            }

            $data[$index]['remaining_quantity'] = $remainingQuantity;
        }

        return $data;
    }
}
